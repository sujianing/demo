# -*- coding: utf-8 -*-
"""
Created on Thu Jul 28 20:26:27 2022

@author: Dell
"""


import random
import re  # 正则表达式
import time
import pandas as pd  # 文件处理
import requests  # 请求数据
from numpy import *  # 创建给定类型的矩阵，并初始化为0
import matplotlib.pyplot as plt  # 数据可视化
import pymysql  # 链接数据库
import matplotlib as mpl  # 绘图库
def get_data():

    for i in range(1, 2):
        print('正在爬取第%d页' % (i))
        # 爬取成都链家网租房信息
        baseurl = 'https://cd.lianjia.com/zufang/pg'
        url = baseurl + str(i) + '/#contentList'
        header = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36'}
        print(url)
        response = requests.get(url, headers=header)
        html = response.text
        data_list = []
        if response.status_code == 200:
            # 单独提取每个小区的信息
            regex = re.compile('title="(.*?)">\n.*?([0-9.]+)㎡\n.*?<em>([0-9.]+)</em> 元/月', re.DOTALL)
            data = regex.findall(html)  # 找出面积
            data = list(data)
            data_list.append(data)
            # print(data_house)
        # 每次随机休息1-5秒
        time.sleep(random.randint(1, 5))
    data_house = pd.DataFrame(data_list)
    return data_house, data_list


def process_data(data):
    res = []
    res_list = []
    for i in range(0, len(data)):
        res.append([data_list[0][i][0].split()[0][3:], data_list[0][i][2]])
        res_list.append([data_list[0][i][0].split()[0][:2],
                         data_list[0][i][0].split()[0][3:],
                         data_list[0][i][0].split()[1],
                         data_list[0][i][0].split()[2],
                         data_list[0][i][1],
                         data_list[0][i][2]]
                        )

    return res, res_list
def query(x, y):
    name = '.*'.join(input('请输入小区名称:'))
    regex = re.compile(name)
    for i in range(0, len(x)):
        match = regex.search(x[i])
        if match:
            print(f'该小区{x[i]}的平均租金为：{y[i]}')


def store_data(data):
    # 连接数据库
    conn = pymysql.connect(
        user="root",
        port=3306,
        passwd="",
        db="spider",
        host="127.0.0.1",
        charset='utf8'
    )
    if conn:
        print('数据库连接成功')
        error = 0
        try:
            cursor = conn.cursor()
            num = 0
            for item in data:
                print(item)
                num = num + 1
                x0 = str(item[0])
                x1 = str(item[1])
                x2 = str(item[2])
                x3 = str(item[3])
                x4 = str(item[4])
                x5 = str(item[5])
                insert_re = f'insert into rent(number, ways,town_name, layout,towards,area,rent) values ({num}, \'{x0}\',\'{x1}\',\'{x2}\',\'{x3}\',\'{x4}\',\'{x5}\')'
                print(insert_re)
                print(type(insert_re))
                cursor.execute(insert_re)
                conn.commit()
        except Exception as e:
            error = error + 1
        except UnicodeDecodeError as e:
            error = error + 1
        # 断开数据库连接
        conn.close()
    else:
        print('数据库连接失败')


# 主函数
if __name__ == '__main__':
    data_house, data_list = get_data()
    print(data_list)
    res, res_list = process_data(data_list[0])
    # save_date(res_list)  # 信息保存到文件
    store_data(res_list)  # 数据存入数据库
    print(res)
    x, y = show(res)
    while True:
        query(x, y)
